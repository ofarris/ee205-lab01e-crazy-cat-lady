///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01e - Crazy Cat Lady - EE 205 - Spr 2022
///
/// @file    crazyCatLady.cpp
/// @version 1.0 - Initial version
///
/// Compile: $ g++ -o crazyCatLady crazyCatLady.cpp
///
/// Usage:  crazyCatLady catName
///
/// Result:
///   Oooooh! [catName] you’re so cute!
///
/// Example:
///   $ ./crazyCatLady Snuggles
///   Oooooh! Snuggles you’re so cute!
///
/// @author  Oze Farris <ofarris@hawaii.edu>
/// @date    18_01_2022
///////////////////////////////////////////////////////////////////////////////


#include <iostream>
#include <cstring>

int main( int argc, char* argv[] ) {
   std::string catName = argv[1];
   std::cout << "Oooooh! "<<catName<< " you're so cute!\n" << std::ends ;
   return 0;
}
